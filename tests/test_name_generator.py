import unittest
from name_generator import *
from univers import dic_universes


class TestNameGeneratorFunctions(unittest.TestCase):
    def test_check_name(self):
        self.assertIsNone(check_name(""))
        self.assertIsNone(check_name("Hello"))

    def test_transformation_Firstname(self):
        self.assertEqual(transformationFirstname(
            dic_universes, "Jojo", "T"), "Hermit")
        self.assertEqual(transformationFirstname(
            dic_universes, "Star Wars", "A"), "Darth")
        self.assertEqual(transformationFirstname(
            dic_universes, "One Piece", "N"), "Nico")
        

    def test_transformation_Lastname(self):
        self.assertEqual(transformationLastname(
            dic_universes, "Jojo", "T"), "Purple")
        self.assertEqual(transformationLastname(
            dic_universes, "Star Wars", "A"), "Bane")
        self.assertEqual(transformationLastname(
            dic_universes, "One Piece", "N"), "Kozuki")
        
