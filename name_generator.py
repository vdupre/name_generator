"""
    The functions module
    =========================

    Use it to import functions about transformations.
"""


def check_name(string):
    """
    Check if the string is not present.

    :param string : string to check
    :type string : str
    :return : none
    """
    if string is None:
        raise Exception("String must not be empty")


def transformationFirstname(dict_universe, universe, letter):
    """
    Match the letter with a firstname in universe.

    :param dict_universe : configuration dictionnary containing universe data
    :type dict_universe : dict
    :param universe : universe name
    :type universe : str
    :param letter : the selected letter
    :type letter : str
    :return : universe firstname
    :rtype : str
    """
    check_name(letter)
    return dict_universe.get(universe).get("dic_letter_firstname").get(letter)


def transformationLastname(dict_universe, universe, letter):
    """
    Match the letter with a lastname in universe.

    :param dict_universe : configuration dictionnary containing universe data
    :type dict_universe : dict
    :param universe : universe name
    :type universe : str
    :param letter : the selected letter
    :type letter : str
    :return : universe lastname
    :rtype : str
    """
    check_name(letter)
    return dict_universe.get(universe).get("dic_letter_lastname").get(letter)
